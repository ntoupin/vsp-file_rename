﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileNaming
{
    class Program
    {
        static long fileProcessed = 0;
        static long fileRenamed = 0;
        static long fileError = 0;

        static long directoryProcessed = 0;
        static long directoryError = 0;
        static long directoryRenamed = 0;

        static List<string> errorList = new List<string>();
        static List<string> caracterList = new List<string>();

        static void Main(string[] args)
        {
            caracterList.Add("%");
            caracterList.Add("#");
            caracterList.Add("+");

            Console.WriteLine("Enter folder path :");
            string path = Console.ReadLine();
            path.Replace("/", "\\");
            path += "\\";

            if (File.Exists(path))
            {
                // This path is a file
                ProcessFile(path);
            }
            else if (Directory.Exists(path))
            {
                // This path is a directory
                ProcessDirectory(path);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", path);
            }

            Console.WriteLine();
            Console.WriteLine("File processed : " + fileProcessed.ToString());
            Console.WriteLine("File renamed : " + fileRenamed.ToString());
            Console.WriteLine("File error : " + fileError.ToString());

            Console.WriteLine("Directory processed : " + directoryProcessed.ToString());
            Console.WriteLine("Directory renamed : " + directoryRenamed.ToString());
            Console.WriteLine("Directory error : " + directoryError.ToString());

            if (fileError > 0)
            {
                Console.WriteLine();
                Console.WriteLine("Error list: ");

                foreach (string element in errorList)
                    Console.WriteLine("Path error : " + element);
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        private static void ProcessDirectory(string targetDirectory)
        {
            //try
            //{
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
                ProcessFile(fileName);

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
            {
                string newPath = null;
                string test = subdirectory;
                try
                {
                    foreach (string element in caracterList)
                    {
                        if (test.Contains(element))
                        {
                            newPath = test.Replace(element, "");

                            // Trim end of string if too long
                            if (newPath.Length >= 240)
                                newPath.Substring(0, 240);

                            Directory.Move(test, newPath);
                            ++directoryRenamed;
                            Console.WriteLine("Renamed folder : " + newPath);
                            test = newPath;
                        }
                        else
                        {
                            Console.WriteLine("Processed folder : " + Path.GetDirectoryName(test));
                            ++directoryProcessed;
                        }
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine("Error processind folder : " + subdirectory + " " + e.Message);
                    ++directoryError;
                    errorList.Add(subdirectory);
                }

                ProcessDirectory(test);
            }
            //}
            //catch
            //{
            //    string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Errors.txt";
            //    StreamWriter sw = File.AppendText(path);
            //    sw.WriteLine(targetDirectory);
            //    sw.Close();
            //}
        }

        private static void ProcessFile(string path)
        {
            string newPath = null;
            try
            {
                foreach (string element in caracterList)
                {
                    if (path.Contains(element))
                    {
                        newPath = path.Replace(element, "");

                        // Trim end of string if too long
                        if (newPath.Length >= 240)
                            newPath.Substring(0, 240);

                        try
                        {
                            File.Move(path, newPath);
                        }
                        catch
                        {
                            Random randNum = new Random();
                            string fileName = Path.GetFileNameWithoutExtension(newPath) + "(" + randNum.Next(1, 100).ToString()
                                + ")" + Path.GetExtension(newPath);
                            string pathOnly = Path.GetDirectoryName(newPath) + "\\";
                            File.Move(path, pathOnly+fileName);
                        }
                        ++fileRenamed;
                        Console.WriteLine("Renamed file : " + newPath);
                        path = newPath;
                    }
                    else
                    {
                        Console.WriteLine("Processed file : " + path);
                        ++fileProcessed;
                    }
                }
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("Error processind file : " + path + " " + e.Message);
                ++fileError;
                errorList.Add(path);
            }
        }
    }
}